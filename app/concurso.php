<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class concurso extends Model
{
    protected $fillable = ['id','nome','edital','dataFinal','dataEncerra'];
    public function getId(){return $this->id;}
    public function getNome(){return $this->nome;}
    public function getDataFinal(){return $this->dataFinal;}
    public function getDataEncerra(){return $this->dataEncerra;}
    public function getEdital(){return $this->edital;}
}
