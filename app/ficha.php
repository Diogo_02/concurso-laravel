<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ficha extends Model
{
    protected $fillable = ['id','id_user','endereco','telefone','dataNasc','instrumento','link','mensagem'];
    public function getId(){return $this->id;}
    public function getIdUser(){return $this->hasOne('App\User','id');}
    public function getEndereco(){return $this->endereco;}
    public function getTelefone(){return $this->telefone;}
    public function getDataNasc(){return $this->dataNasc;}
    public function getInstrumento(){return $this->instrumento;}
    public function getLink(){return $this->link;}
    public function getMensagem(){return $this->mensagem;}
}
