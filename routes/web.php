<?php
Auth::routes();
Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware'=> ['web','auth']], function(){
    Route::get('/home', function(){
        if (Auth::user()->nivel == 0) {
            $users = Auth::user()->id;
            $user = App\Ficha::all();
            $u = $user[0]->id_user; 
            $con = App\Concurso::all();
            return view('home', compact('users','u','con'));
        } else if (Auth::user()->nivel == 1){
            $users = App\User::all(); 
            $con = App\Concurso::all();
            return view('adminhome', compact('users','con'));
        }else if(Auth::user()->nivel == 3){
            $Juser = App\User::all(); 
            return view('juradohome');
        }else{
            return "algo está errado";
        }
    });
});
//ADMIN
    Route::resource('admin', 'AdminController')->middleware('auth');
    //GERENCIAR USUARIOS
    route::get('/adm/Participantes', function(){
        $users = App\User::all();
        return view('adminPart', compact('users'));
    })->name('adminParticipantes');
    //CONCURSO
    route::get('/adm/CriarConcurso', function(){
        return view('concurso');
    })->name('concurso');
    route::get('/adm/Jurados', function(){
        $Juser = App\User::all(); 
        return view('juradoConcurso', compact('Juser'));
    })->name('ConcJur');
    Route::POST("/home/Concurso", "ConcursoController@create")->name("CriarConcurso");
    route::get("/home/envioJu", "ConcursoController@InserirJurado")->name("InJu");
//FIM ADMIN

//PARTICIPANTE
    route::get("/home/ficha", function(){
        $users = Auth::user()->id;
        //$ficha = \App\ficha::getId();
        return view("ficha",compact('users'));
    })->name("ficha");

    Route::POST("/home/Fichas", "ParticipanteController@create")->name("Criarficha");
//FIM PARTICIPANTE

//JURADO

//FIM JURADO
//Route::post('ficha/cria','Controller@criarFicha')->name('criarJurado');