@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Todos os usuários cadastrados</div>
                <div class="card-body">
                    <table class='table table-hover table-bordered'>
                        <thead>
                            <tr>
                                <th width='5'>Nº</th>
                                <th>Nome </th>
                                <th>Email</th>
                                <th>Nível</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $key => $value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->name}}</td>
                                <td>{{$value->email}}</td>
                                <td>{{$value->nivel}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
