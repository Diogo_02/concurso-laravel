@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Ficha de inscrição ') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('Criarficha')}}">
                        @csrf
                        <!--Endereço -->
                        <div class="form-group row">
                            <label for="endereco" class="col-md-4 col-form-label text-md-right">{{ __('Endereço') }}</label>
                            <div class="col-md-6">
                                <input id="endereco" type="text" class="form-control{{ $errors->has('endereco') ? ' is-invalid' : '' }}" name="endereco" value="{{ old('endereco') }}" required autofocus>
                                <input id="id" type="hidden" class="form-control" name="id_user" value="{{$users}}">
                                @if ($errors->has('endereco'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('endereco') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Telefone -->
                        <div class="form-group row">
                            <label for="telefone" class="col-md-4 col-form-label text-md-right">{{ __('Telefone') }}</label>

                            <div class="col-md-6">
                                <input id="telefone" type="text" class="form-control{{ $errors->has('telefone') ? ' is-invalid' : '' }}" name="telefone" required>

                                @if ($errors->has('telefone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telefone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Data de Nascimento -->
                        <div class="form-group row">
                            <label for="dataNasc" class="col-md-4 col-form-label text-md-right">{{ __('Data de Nascimento') }}</label>
                            <div class="col-md-6">
                                <input id="dataNasc" type="date" class="form-control{{ $errors->has('dataNasc') ? ' is-invalid' : '' }}" name="dataNasc" required>
                                @if ($errors->has('dataNasc'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('dataNasc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Instrumento -->
                        <div class="form-group row">
                            <label for="instrumento" class="col-md-4 col-form-label text-md-right">{{ __('Instrumento') }}</label>

                            <div class="col-md-6">
                                <input id="instrumento" type="text" class="form-control" name="instrumento" required>
                            </div>
                        </div>
                        <!-- Link -->
                        <div class="form-group row">
                            <label for="link" class="col-md-4 col-form-label text-md-right">{{ __('Link do Youtube') }}</label>

                            <div class="col-md-6">
                                <input id="link" type="link" class="form-control" name="link" required>
                            </div>
                        </div>
                        <!-- Mensagem -->
                        <div class="form-group row">
                            <label for="mensagem" class="col-md-4 col-form-label text-md-right">{{ __('Mensagem para comissão') }}</label>

                            <div class="col-md-6">
                                <input id="mensagem" type="text" class="form-control" name="mensagem" required>
                            </div>
                        </div>
                        <!-- Criar ficha -->
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Criar ficha') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
