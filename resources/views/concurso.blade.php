@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('CriarConcurso')}}">
                        @csrf
                        <!--Nome -->
                        <div class="form-group row">
                            <label for="nome" class="col-md-4 col-form-label text-md-right">{{ __('Nome do Concurso') }}</label>
                            <div class="col-md-6">
                                <input id="nome" type="text" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" name="nome" value="{{ old('nome') }}" required autofocus>
                                @if ($errors->has('nome'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nome') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- edital -->
                        <div class="form-group row">
                            <label for="edital" class="col-md-4 col-form-label text-md-right">{{ __('Edital') }}</label>
                            <div class="col-md-6">
                                <input id="edital" type="text" class="form-control{{ $errors->has('edital') ? ' is-invalid' : '' }}" name="edital" required>
                                @if ($errors->has('Edital'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Edital') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Data de Encerra -->
                        <div class="form-group row">
                            <label for="dataEncerra" class="col-md-4 col-form-label text-md-right">{{ __('Data de Encerramento') }}</label>
                            <div class="col-md-6">
                                <input id="dataEncerra" type="date" class="form-control{{ $errors->has('dataEncerra') ? ' is-invalid' : '' }}" name="dataEncerra" required>
                                @if ($errors->has('dataEncerra'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('dataEncerra') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Data de Final -->
                        <div class="form-group row">
                            <label for="dataFinal" class="col-md-4 col-form-label text-md-right">{{ __('Data Final') }}</label>
                            <div class="col-md-6">
                                <input id="dataFinal" type="date" class="form-control{{ $errors->has('dataFinal') ? ' is-invalid' : '' }}" name="dataFinal" required>
                                @if ($errors->has('dataFinal'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('dataFinal') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Criar Concurso -->
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Criar concurso') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <a href="{{route('admin.index')}}" > Voltar </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
