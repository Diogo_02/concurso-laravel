@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class='row'>
            <div class="card">
                <div class="card-header">Admin funções</div>
                    <a class="btn" href="{{route('adminParticipantes')}}">Criar Participante/Jurados</button><br>
                    <a class="btn" href="{{route('adminParticipantes')}}">Gerenciar Usuários</a><br>
                    <a class="btn" href="{{route('concurso')}}">Criar concurso</a><br>
                    <!--<button class="btn">Concursos </button>-->
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="alert alert-success" role="alert">
                        <p> Você está logado como Administrador.</p>
                    </div>
                    <table class='table table-hover table-bordered'>
                        <thead>
                            <tr>
                                <th width='5'>Nº</th>
                                <th>Nome </th>
                                <th>Email</th>
                                <th>Nível</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $key => $value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->name}}</td>
                                <td>{{$value->email}}</td>
                                <td>{{$value->nivel}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @foreach ($con as $key => $value )
                <div class='card'>
                <div class="card-header">Concurso {{$value->nome}}</div>
                <div class="card-body">
                    <div class="alert alert-warning" role="alert">         
                        Veja o edital <a href="{{$value->edital}}">Clique aqui</a>                       
                        <p>Data de encerramento:{{$value->dataEncerra}}</p>
                        <p>Data de final:{{$value->dataFinal}}</p>
                        <a class='btn' href="{{route('ConcJur')}}" > Ver mais informações </a >
                    </div>
                </div>
                </div>
            @endforeach
            </div>
        
    </div>
</div>
@endsection
