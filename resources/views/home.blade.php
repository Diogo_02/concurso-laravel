@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif  
                    <div class="alert alert-success" role="alert">
                        <p> Você está logado como participante.</p>
                    </div>
                    <div class="alert alert-warning" role="alert">
                    @if($u == $users)
                        voce ja criou a ficha 
                    @else
                        Precisa criar sua ficha de participante. <a href="{{route('ficha')}}">Clique aqui</a>
                    @endif
                    </div>
                    
                    @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    Você jé está cadastro no concurso
                                </div>
                            @endif

                    <!-- Card Concurso-->
                    <div class="card col-6">
                        @foreach ($con as $key => $value )
                            <div class="card-header">Concurso {{$value->nome}}</div>
                            <div class="card-body">
                                <div class="alert alert-warning" role="alert">         
                                    Veja o edital <a href="{{$value->edital}}">Clique aqui</a>                       
                                    <p>Data de encerramento:{{$value->dataEncerra}}</p>
                                    <p>Data de final:{{$value->dataFinal}}</p>

                                    <input type='submit' class='btn btn-success' name='enviar' value='enviar'>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
