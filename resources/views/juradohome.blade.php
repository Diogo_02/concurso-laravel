@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="alert alert-success" role="alert">
                        <p> Você está logado como jurado.</p>
                    </div>
                    <table class='table table-bordered table-dark'>
                        <tr>
                            <th>ID</th>
                            <th>Participante</th>
                            <th>Qua participante</th>
                            <th>Nota</th>
                        </tr>
                        
                        @foreach ($Juser as $key => $value )
                        <tr>
                            <td>{{$value->id}}</td>
                            <td>{{$value->name}}</td>
                            <td><input type='radio' name='selecao'></td>
                            <td><input type='number' name='nota'></td>
                        </tr>
                        @endforeach
                        

                    <table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
